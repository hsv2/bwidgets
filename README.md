# sdl2_basic_widgets

A (very) basic and incomplete widget library for SDL applications.

## Build
The build dependencies are SDL2, SDL_ttf and fontconfig dev libs
and headers. Meson is used as build system, so you'll need it too.

As the code use some C++20 features, you'll need a recent compiler
which support that version of the standard.

Then, inside the project root directory, run the following commands:

```ShellSession
$ meson build
$ meson compile -C build -j0
```

After that you'll have in the build directory the static library and the
example programs.

## API overview

The library API is divided in two main parts:
- the core, which provides some basic wrapping of
  SDL, SDL_ttf and fontconfig functionalities.
- the widget API built on top of the core.

### Core

The main classes of the core are:

- [Font](inc/basic_widgets/core/font.hpp):
  wraps TTF_Font object with the most common operations
  of SDL_ttf. Also provide a helper function to
  search system fonts by using fontconfig.
- [Renderer](inc/basic_widgets/core/renderer.hpp):
  wraps SDL_Renderer object with common operation from
  the SDL 2D accelerated rendering API.
- [Texture](inc/basic_widgets/core/texture.hpp):
  wraps SDL_Texture object with common operations
  to manipulate it.

Apart from the SDL API wrappers, some generic utility functions and
data types are defined:

- [Color](inc/basic_widgets/core/type/color.hpp):
  wraps SDL_Color object and adds convenience operators
  overloads to operate on all color channels and/or
  alpha channel.
- [draw.hpp](inc/basic_widgets/core/draw.hpp):
  2D drawing utilities. Currently, provides a couple of basic utility
  functions for software rendered textures: circle rendering
  and an antialising algorithm.
- [math.hpp](inc/basic_widgets/core/math.hpp): mostly provides
  frequently used formulas to work with rectangles and points
  on a 2D coordinate system.
- [concepts.hpp](inc/basic_widgets/core/type/concepts.hpp):
  a few concept definitions for used for generic programming.
- [Size](inc/basic_widgets/core/type/size.hpp): representation
  of a 2D size. There are also a few operator overloads to
  operate on Size objects.

## Widgets

Widgets are classes that implements the common widget interface and
optionally one or more "handler" interfaces. Handlers are interfaces
for event or external resources handling.

These interfaces are:

- [Widget](inc/basic_widgets/w/base/widget.hpp): the common base
  interface of every widget.
- [EventHandler](inc/basic_widgets/w/feat/event_handler.hpp): base
  interface for event handlers.
- [FocusHandler](inc/basic_widgets/w/feat/focus_handler.hpp): base
  interface for handler having a focus notion.
- [KeyboardHandler](inc/basic_widgets/w/feat/keyboard_handler.hpp):
  interface for key and text input events handling.
- [MouseHandler](inc/basic_widgets/w/feat/mouse_handler.hpp):
  interface for mouse events handling. Handle click and focus state
  changes and also introduce a notion of "pushed" state.
- [FontHandler](inc/basic_widgets/w/feat/font_handler.hpp): interface
  for widgets using a font. Declares methods to set the widget font and
  set background and text color used for text rendering.
- [TextureHandler](inc/basic_widgets/w/feat/texture_handler.hpp): it exists
  but I'm thinking now that it shouldn't.

Based on these interfaces, there's implementation for a few basic widgets.
Currently, the following ones are defined:

- [Button](inc/basic_widgets/w/button.hpp): a push button.
- [Caption](inc/basic_widgets/w/caption.hpp): a caption
  that displays a rendered string. No multiline support.
- [Input<T>](inc/basic_widgets/w/base/input.hpp): base
  class for widgets using an input field.
- [NumericInput<Numeric>](inc/basic_widgets/w/numeric_input.hpp) an
  input widget for numeric values with two buttons to increase/decrease
  the hold value.

There are also "containers" widgets which are widget that hold a collection of
widgets and manage their geometry:

- [Layout](inc/basic_widgets/w/base/layout.hpp): abstract class for container widgets.
- [AlignedLayout](inc/basic_widgets/w/aligned_layout.hpp): layout that is done by distributing
  widgets on a horizontal or vertical row.

To create widget objects, the corresponding function from [widget_factory.hpp](inc/basic_widgets/w/widget_factory.hpp)
should be used. It's done this way for interface-implementation sake.

## Source code organization overview

The header and source directory hierarchies is organized in the following way:

- core: C→C++ wrappers and commonly used generic functions.
- core/type: basic data types and concepts.
- w: the widget library
- w/base: the abstract widget classes.
- w/feat: handlers (optional widget features)

Widget code intent to separate public API from implementation details. For that
a Widget has two classes, a pure virtual one (Widget) which is the public API and another
class (WidgetImpl) that implements the interface of the first one. Unless an access
to implementation details is required, the interface (pure virtual) type should
be used.
