project('sdl2_basic_widgets', 'cpp',
    version : '0.9.0',
    default_options : [
        'b_lundef=false',
        'b_sanitize=undefined',
        'cpp_std=c++20',
        'optimization=g',
        'warning_level=3',
    ],
    license: 'EUPL-1.2')

add_project_arguments('-Wconversion', '-pedantic', language: 'cpp')

if (get_option('buildtype').startswith('debug'))
    add_project_arguments('-DBWIDGETS_DEBUG', language: 'cpp')
endif

sdl = [
    dependency('sdl2', version: '>=2.0.5'),
    dependency('SDL2_ttf')
    ]
fontconfig = dependency('fontconfig')

pub_api = include_directories('inc')

libbasic_widgets = static_library('basic_widgets',
    'src/core/draw.cpp',
    'src/core/font.cpp',
    'src/core/renderer.cpp',
    'src/core/texture.cpp',
    'src/w/base/layout_impl.cpp',
    'src/w/base/widget_impl.cpp',
    'src/w/button_impl.cpp',
    'src/w/caption_impl.cpp',
    'src/w/feat/event_handler_impl.cpp',
    'src/w/feat/keyboard_handler_impl.cpp',
    'src/w/feat/mouse_handler_impl.cpp',
    'src/w/widget_factory.cpp',
    dependencies : [sdl, fontconfig],
    include_directories : pub_api,
    install : true)

libbasic_widgets_dep = declare_dependency(
    include_directories : pub_api,
    link_with: libbasic_widgets,
    dependencies : [sdl, fontconfig])

executable('button_demo',
    'examples/button_example.cpp',
    dependencies: [sdl],
    include_directories : pub_api,
    link_with : libbasic_widgets,
    install : false)

executable('caption_demo',
    'examples/caption_example.cpp',
    dependencies: [sdl],
    include_directories : pub_api,
    link_with : libbasic_widgets,
    install : false)

executable('example_demo',
    'examples/example_example.cpp',
    dependencies: [sdl],
    include_directories : pub_api,
    link_with : libbasic_widgets,
    install : false)

executable('input_demo',
    'examples/input_example.cpp',
    dependencies: [sdl],
    include_directories : pub_api,
    link_with : libbasic_widgets,
    install : false)
