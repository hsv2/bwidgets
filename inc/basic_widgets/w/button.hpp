#ifndef BWIDGETS_BUTTON_HPP
#define BWIDGETS_BUTTON_HPP

#include <string>

#include <basic_widgets/core/type/color.hpp>
#include <basic_widgets/w/base/widget.hpp>
#include <basic_widgets/w/feat/font_handler.hpp>
#include <basic_widgets/w/feat/mouse_handler.hpp>

namespace bwidgets
{
    class Button : public virtual Widget,
                   public virtual FontHandler,
                   public virtual MouseHandler
    {
    public:
        static inline const Color default_color_bg {150, 150, 150, SDL_ALPHA_OPAQUE};
        static inline const Color default_color_bg_hover {175, 175, 175,
                                                          SDL_ALPHA_OPAQUE};
        static inline const Color default_color_fg {0, 0, 0, SDL_ALPHA_OPAQUE};

        std::function<Color(int, int, float)> border_gradient;
        Size                                  border_size {3, 3};
        Color                                 color_bg       = default_color_bg;
        Color                                 color_bg_hover = default_color_bg_hover;

        using Widget::Widget;

        // Get button text (label).
        [[nodiscard]] virtual auto text() const noexcept -> std::string_view = 0;
        // Set button text
        virtual void               text(std::string)                         = 0;
    };
}

#endif
