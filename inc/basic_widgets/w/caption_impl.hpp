#ifndef BWIDGETS_CAPTION_IMPL_HPP
#define BWIDGETS_CAPTION_IMPL_HPP

#include <basic_widgets/w/base/widget_impl.hpp>
#include <basic_widgets/w/caption.hpp>
#include <basic_widgets/w/feat/font_handler_impl.hpp>

namespace bwidgets
{
    class CaptionImpl : public virtual Caption,
                        public virtual FontHandlerImpl,
                        public virtual WidgetImpl
    {
    public:
        CaptionImpl(Widget* p = nullptr) noexcept;

        void               render_mode(Font::RenderMode) override;
        [[nodiscard]] auto size() const noexcept -> Size override;
        [[nodiscard]] auto text() const noexcept -> std::string_view override;
        void               text(std::string) override;

    protected:
        Font::RenderMode         _render_mode {Font::RenderMode::SHADED};
        std::string              _text;
        std::shared_ptr<Texture> _text_texture {nullptr};

        void _handle_font_change(const std::shared_ptr<Font>&) override;
        void _handle_font_color_change(Color, Color)
        override;
        void _handle_geometry_change(const SDL_Rect&) override;
        void _handle_renderer_change(const std::shared_ptr<Renderer>&) override;
        void _handle_rendering() override;
        void _handle_texture_update() override;
    };
}

#endif
