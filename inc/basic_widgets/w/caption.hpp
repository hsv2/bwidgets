#ifndef BWIDGETS_CAPTION_HPP
#define BWIDGETS_CAPTION_HPP

#include <basic_widgets/w/base/widget.hpp>
#include <basic_widgets/w/feat/font_handler.hpp>
#include <basic_widgets/w/feat/texture_handler.hpp>

struct SDL_Surface;

namespace bwidgets
{
    class Caption : public virtual Widget,
                    public virtual FontHandler,
                    public virtual TextureHandler
    {
    public:
        enum struct Alignment
        {
            CENTER,
            LEFT,
            RIGHT
        };
        inline static const Color default_color_bg = {255, 255, 255, SDL_ALPHA_OPAQUE};
        inline static const Color default_color_fg = {0, 0, 0, SDL_ALPHA_OPAQUE};
        inline static const Size  default_margins  = {3, 3};

        // Text horizontal alignment on the caption viewport.
        Alignment alignment {Alignment::LEFT};
        Size      margins {default_margins};

        using Widget::Widget;

        // Set caption text rendering mode.
        virtual void               render_mode(Font::RenderMode)             = 0;
        // Get caption text.
        [[nodiscard]] virtual auto text() const noexcept -> std::string_view = 0;
        // Set caption text.
        virtual void               text(std::string)                         = 0;
    };
}

#endif
