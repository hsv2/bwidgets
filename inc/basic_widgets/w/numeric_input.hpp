#ifndef BWIDGETS_NUMERIC_INPUT_HPP
#define BWIDGETS_NUMERIC_INPUT_HPP

#include <basic_widgets/core/type/concepts.hpp>
#include <basic_widgets/w/base/input.hpp>

namespace bwidgets
{
    template<Numeric T>
    class NumericInput : public virtual Input<T>
    {
    protected:
        using Input<T>::Input;

    public:
        // Button increment/decrement step.
        T button_step = 1;

        // Get allowed value range.
        [[nodiscard]] virtual auto value_range() const noexcept
          -> const std::pair<T, T>&            = 0;
        // Set allowed value range.
        virtual void value_range(T min, T max) = 0;
    };
}

#endif
