#ifndef BWIDGETS_LAYOUT_IMPL_HPP
#define BWIDGETS_LAYOUT_IMPL_HPP

#include <vector>

#include <basic_widgets/w/base/layout.hpp>
#include <basic_widgets/w/base/widget_impl.hpp>

namespace bwidgets
{
    class Renderer;
}

namespace bwidgets
{
    class LayoutImpl : public virtual Layout,
                       public virtual WidgetImpl
    {
    public:
        using WidgetImpl::WidgetImpl;

        void add_widget(std::unique_ptr<Widget>) override;
        void for_widgets(const std::function<void(Widget*)>&) override;
        void handle_event(const SDL_Event&) override;

    protected:
        std::vector<std::unique_ptr<Widget>> _widgets;

        void _handle_geometry_change(const SDL_Rect&) override;
        void _handle_renderer_change(const std::shared_ptr<Renderer>&) override;
        void _handle_rendering() override;

        // Compute and set widget geometry.
        virtual void _update_layout(const SDL_Rect&) = 0;
    };
}

#endif
