#ifndef BWIDGETS_INPUT_HPP
#define BWIDGETS_INPUT_HPP

#include <basic_widgets/core/type/concepts.hpp>
#include <basic_widgets/w/base/widget.hpp>
#include <basic_widgets/w/feat/font_handler.hpp>
#include <basic_widgets/w/feat/keyboard_handler.hpp>
#include <basic_widgets/w/feat/mouse_handler.hpp>

namespace bwidgets
{

    template<typename T>
    class Input : public virtual Widget,
                  public virtual FontHandler,
                  public virtual KeyboardHandler,
                  public virtual MouseHandler
    {
    protected:
        using Widget::Widget;

    public:
        static const int          default_border_width = 3;
        inline static const Color default_color_bg = {200, 200, 200, SDL_ALPHA_OPAQUE};
        inline static const Color default_color_bg_focused = {255, 255, 255,
                                                              SDL_ALPHA_OPAQUE};
        static const int          default_float_precision  = 2;
        static const int          default_min_width        = 1;

        int   border_width     = default_border_width;
        Color color_bg_default = default_color_bg;
        Color color_bg_focused = default_color_bg_focused;
        int   float_precision  = default_float_precision;
        int   input_min_width  = default_min_width;
        char  input_width_unit = 'W'; // char used as unit for text length computation.
        T     value {};

        // Get the current displayed string.
        [[nodiscard]] virtual auto input_text() const -> std::string_view = 0;
        // Set displayed string
        virtual void               input_text(std::string)                = 0;
        virtual void               input_text_color(Color)                = 0;
        // Check if a character is allowed to be inputted. String are used because we
        // could have multibytes characters to represent using 8bits chars.
        [[nodiscard]] virtual auto is_valid_input(std::string_view) const noexcept
          -> bool = 0;

        // Process a value for instance to modify it before saving it.
        [[nodiscard]] virtual auto process_value(T) const noexcept -> T = 0;
        // Get string representation to be shown of the stored value.
        [[nodiscard]] virtual auto value_from_string(std::string_view) const noexcept
          -> T                                                                      = 0;
        // Parse a string to get its representation in T type of input value memory.
        [[nodiscard]] virtual auto value_to_string(T) const noexcept -> std::string = 0;
    };
}

#endif
