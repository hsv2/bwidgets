#ifndef BWIDGETS_WIDGET_IMPL_HPP
#define BWIDGETS_WIDGET_IMPL_HPP

#include <basic_widgets/w/base/widget.hpp>
#include <basic_widgets/w/feat/event_handler_impl.hpp>

namespace bwidgets
{
    class WidgetImpl : public virtual Widget,
                       public virtual EventHandlerImpl
    {
    public:
        void               render() override;
        void               renderer(std::shared_ptr<Renderer>) override;
        void               viewport(const SDL_Rect&) override;
        [[nodiscard]] auto viewport() const -> const SDL_Rect& override;

    protected:
        std::shared_ptr<Renderer> _renderer;
        SDL_Rect                  _viewport {0, 0, 0, 0};
        // Area actually used by the widget.
        SDL_Rect                  _widget_area {0, 0, 0, 0};

        using Widget::Widget;

        // Called on viewport change.
        virtual void _handle_geometry_change(const SDL_Rect&) = 0;
        // Called on renderer change.
        virtual void _handle_renderer_change(const std::shared_ptr<Renderer>&) {}
        // Called on rendering.
        virtual void _handle_rendering() = 0;
    };
}

#endif
