#ifndef BWIDGETS_LAYOUT_HPP
#define BWIDGETS_LAYOUT_HPP

#include <basic_widgets/core/type/size.hpp>
#include <basic_widgets/w/base/widget.hpp>

namespace bwidgets
{
    class Layout : public virtual Widget
    {
    protected:
        using Widget::Widget;

    public:
        static inline const Size default_margins {8, 8};

        // Margins between layout widgets.
        Size margins = default_margins;

        // Add widget to the layout
        virtual void add_widget(std::unique_ptr<Widget>)              = 0;
        // Apply a function to every layout widget.
        virtual void for_widgets(const std::function<void(Widget*)>&) = 0;
    };
}

#endif
