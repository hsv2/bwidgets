#ifndef BWIDGETS_WIDGET_HPP
#define BWIDGETS_WIDGET_HPP

#include <memory>

#include <basic_widgets/core/renderer.hpp>
#include <basic_widgets/core/type/size.hpp>
#include <basic_widgets/w/feat/event_handler.hpp>

union SDL_Event;

struct SDL_Renderer;

namespace bwidgets
{
    class Widget : public virtual EventHandler
    {
    public:
        // Parent widget for bidirectional message passing between widgets.
        // Currently not used by this lib.
        Widget* parent;

        explicit Widget(Widget* p = nullptr) noexcept : parent {p} {}

        // Render widget on current renderer target.
        virtual void               render()                            = 0;
        // Set widget renderer.
        virtual void               renderer(std::shared_ptr<Renderer>) = 0;
        // Get prefered or minimal widget size.
        [[nodiscard]] virtual auto size() const noexcept -> Size       = 0;
        // Set the viewport delimiting the widget rendering area and the origin
        // for relative coordinates.
        virtual void               viewport(const SDL_Rect&)           = 0;
        // Get current viewport.
        [[nodiscard]] virtual auto viewport() const -> const SDL_Rect& = 0;
    };
}

#endif
