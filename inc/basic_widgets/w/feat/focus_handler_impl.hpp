#ifndef BWIDGETS_FOCUS_HANDLER_IMPL_HPP
#define BWIDGETS_FOCUS_HANDLER_IMPL_HPP

#include <basic_widgets/w/feat/focus_handler.hpp>

namespace bwidgets
{
    class FocusHandlerImpl : public virtual FocusHandler
    {
        bool                      _has_focus {false};
        std::function<void(bool)> _focus_handler {[](auto) {}};

    public:
        void focus(const bool _focus) override
        {
            if (_has_focus != _focus) _focus_handler(_has_focus = _focus);
        }

        bool focus() override
        {
            return _has_focus;
        }

        void focus_handler(decltype(_focus_handler) handler) final
        {
            _focus_handler = std::move(handler);
        }
    };
}

#endif
