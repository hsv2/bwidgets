#ifndef BWIDGETS_MOUSE_HANDLER_HPP
#define BWIDGETS_MOUSE_HANDLER_HPP

#include <basic_widgets/w/feat/event_handler.hpp>
#include <basic_widgets/w/feat/focus_handler.hpp>

struct SDL_MouseButtonEvent;
struct SDL_MouseMotionEvent;

namespace bwidgets
{
    class MouseHandler : public virtual FocusHandler,
                         public virtual EventHandler
    {
    protected:
        using EventHandler::EventHandler;

    public:
        // Set click handler.
        virtual void click_handler(std::function<void(const SDL_MouseButtonEvent&)>) = 0;
        // Disable mouse event handling.
        virtual void disable_mouse_handler()                                         = 0;
        // Enable mouse event handling.
        virtual void enable_mouse_handler(const SDL_Rect&, const SDL_Rect&)          = 0;
        // Called on hover state changes.
        virtual void hover_handler(std::function<void(bool)>)                        = 0;
        // Get current hover state.
        [[nodiscard]] virtual bool hovered() const                                   = 0;
        // Set mouse motion handler.
        virtual void
        mouse_motion_handler(std::function<void(const SDL_MouseMotionEvent&)>) = 0;
        // Get current push state (mouse button down)
        [[nodiscard]] virtual bool pushed() const                              = 0;
    };
}

#endif
