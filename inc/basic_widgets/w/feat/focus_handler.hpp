#ifndef BWIDGETS_FOCUS_HANDLER_HPP
#define BWIDGETS_FOCUS_HANDLER_HPP

#include <functional>

struct SDL_Rect;

namespace bwidgets
{
    class FocusHandler
    {
    public:
        FocusHandler()                    = default;
        FocusHandler(const FocusHandler&) = delete;
        FocusHandler(FocusHandler&&)      = delete;
        auto operator=(FocusHandler&&) = delete;
        auto operator=(const FocusHandler&) = delete;

        virtual ~FocusHandler()                               = default;
        // set focus state.
        virtual void focus(bool focus)                        = 0;
        // get focus state.
        virtual bool focus()                                  = 0;
        // set an handler for focus states changes.
        virtual void focus_handler(std::function<void(bool)>) = 0;
    };
}

#endif
