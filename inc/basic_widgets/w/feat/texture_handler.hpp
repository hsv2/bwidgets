#ifndef BWIDGETS_TEXTURE_HANDLER_HPP
#define BWIDGETS_TEXTURE_HANDLER_HPP

namespace bwidgets
{
    class Renderer;
}

namespace bwidgets
{
    class TextureHandler
    {
    protected:
        TextureHandler() noexcept             = default;
        // (Re)render textures.
        virtual void _handle_texture_update() = 0;

    public:
        TextureHandler(const TextureHandler&) = delete;
        TextureHandler(TextureHandler&&)      = delete;
        virtual ~TextureHandler() noexcept    = default;

        auto operator=(const TextureHandler&) = delete;
        auto operator=(TextureHandler&&) = delete;
    };
}

#endif
