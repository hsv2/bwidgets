#ifndef BWIDGETS_EVENT_HANDLER_HPP
#define BWIDGETS_EVENT_HANDLER_HPP

#include <functional>

#include <SDL_events.h>

namespace bwidgets
{
    class EventHandler
    {
    protected:
        EventHandler() = default;

    public:
        using handler_t =
          std::pair<SDL_EventType, std::function<void(const SDL_Event&)>>;

        EventHandler(const EventHandler&) = delete;
        EventHandler(EventHandler&&)      = delete;
        auto operator=(const EventHandler&) -> EventHandler& = delete;
        auto operator=(EventHandler&&) -> EventHandler& = delete;

        virtual ~EventHandler() = default;

        // Pass the event to be handled.
        virtual void handle_event(const SDL_Event&) = 0;
    };
}

#endif
