#ifndef BWIDGETS_MOUSE_HANDLER_IMPL_HPP
#define BWIDGETS_MOUSE_HANDLER_IMPL_HPP

#include <basic_widgets/w/feat/event_handler_impl.hpp>
#include <basic_widgets/w/feat/focus_handler_impl.hpp>
#include <basic_widgets/w/feat/mouse_handler.hpp>

namespace bwidgets
{
    class MouseHandlerImpl : public virtual MouseHandler,
                             public virtual EventHandlerImpl,
                             public virtual FocusHandlerImpl
    {
        bool _hovered {false};
        bool _pushed {false};

        std::function<void(const SDL_MouseButtonEvent&)> _click_handler {[](auto) {}};
        std::function<void(bool)>                        _hover_handler {[](auto) {}};
        std::function<void(const SDL_MouseMotionEvent&)> _motion_handler {[](auto) {}};

    public:
        void click_handler(decltype(_click_handler) handler) override
        {
            _click_handler = std::move(handler);
        }

        void disable_mouse_handler() override;

        void enable_mouse_handler(const SDL_Rect&, const SDL_Rect&) override;

        [[nodiscard]] bool hovered() const override
        {
            return _hovered;
        }

        void hover_handler(decltype(_hover_handler) handler) override
        {
            _hover_handler = std::move(handler);
        }

        void mouse_motion_handler(decltype(_motion_handler) handler) override
        {
            _motion_handler = std::move(handler);
        }

        [[nodiscard]] bool pushed() const override
        {
            return _pushed;
        }

    protected:
        using MouseHandler::MouseHandler;

        // Called on push state changes.
        virtual void _on_push(bool) {}
    };
}

#endif
