#ifndef BWIDGETS_EVENT_HANDLER_IMPL_HPP
#define BWIDGETS_EVENT_HANDLER_IMPL_HPP

#include <unordered_map>

#include <basic_widgets/w/feat/event_handler.hpp>

namespace bwidgets
{
    class EventHandlerImpl : public virtual EventHandler
    {
        std::unordered_map<SDL_EventType, std::function<void(const SDL_Event&)>>
          _event_handlers {};

    public:
        void handle_event(const SDL_Event&) override;

    protected:
        // Add an handler for a given event type. There can be only one handler by
        // event type. Return true if handler is added successfuly. Return false
        // if handler cannot be added because there's already an handler for that
        // event type.
        auto _add_event_handler(handler_t) -> bool;
        // Remove the handler of a given event type. Return true with the removed
        // handler on success, false otherwise.
        auto _remove_event_handler(SDL_EventType) -> std::pair<handler_t, bool>;
    };
}

#endif
