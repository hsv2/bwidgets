#ifndef BWIDGETS_FONT_HANDLER_HPP
#define BWIDGETS_FONT_HANDLER_HPP

#include <basic_widgets/core/font.hpp>

namespace bwidgets
{
    class FontHandler
    {
    public:
        inline static const Color default_font_color_bg {255, 255, 255,
                                                         SDL_ALPHA_OPAQUE};
        inline static const Color default_font_color_fg {0, 0, 0, SDL_ALPHA_OPAQUE};

        FontHandler(const FontHandler&) = delete;
        FontHandler(FontHandler&&)      = delete;
        auto operator=(const FontHandler&) = delete;
        auto operator=(FontHandler&&)   = delete;
        virtual ~FontHandler() noexcept = default;

        // Set the used font.
        virtual void font(std::shared_ptr<Font> f) = 0;
        // Set background color used for shaded text rendering
        // mode.
        virtual void font_color_bg(Color c)        = 0;
        // Set foreground (glyphs) color.
        virtual void font_color_fg(Color c)        = 0;

    protected:
        FontHandler() noexcept = default;
    };
}

#endif
