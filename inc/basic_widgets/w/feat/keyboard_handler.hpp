#ifndef BWIDGETS_KEYBOARD_HANDLER_HPP
#define BWIDGETS_KEYBOARD_HANDLER_HPP

#include <basic_widgets/w/feat/event_handler.hpp>
#include <basic_widgets/w/feat/focus_handler.hpp>

struct SDL_KeyboardEvent;
struct SDL_TextInputEvent;

namespace bwidgets
{
    class KeyboardHandler : public virtual EventHandler,
                            public virtual FocusHandler
    {
    protected:
        using EventHandler::EventHandler;

    public:
        // Disable keyboard event handling.
        virtual void disable_keyboard_handler()                                 = 0;
        // Enable keyboard event handling.
        virtual void enable_keyboard_handler()                                  = 0;
        // Set the handler for key press events.
        virtual void key_handler(std::function<void(const SDL_KeyboardEvent&)>) = 0;
        // Set the handler for text input events.
        virtual void
        text_input_handler(std::function<void(const SDL_TextInputEvent&)>) = 0;
    };
}

#endif
