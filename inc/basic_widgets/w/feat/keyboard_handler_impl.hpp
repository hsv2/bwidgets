#ifndef BWIDGETS_KEYBOARD_HANDLER_IMPL_HPP
#define BWIDGETS_KEYBOARD_HANDLER_IMPL_HPP

#include <basic_widgets/w/feat/event_handler_impl.hpp>
#include <basic_widgets/w/feat/focus_handler_impl.hpp>
#include <basic_widgets/w/feat/keyboard_handler.hpp>

namespace bwidgets
{
    class KeyboardHandlerImpl : public virtual KeyboardHandler,
                                public virtual EventHandlerImpl,
                                public virtual FocusHandlerImpl
    {
        std::function<void(const SDL_KeyboardEvent&)>  _key_handler {[](auto) {}};
        std::function<void(const SDL_TextInputEvent&)> _input_handler {[](auto) {}};

    public:
        void disable_keyboard_handler() override;
        void enable_keyboard_handler() override;
        void key_handler(decltype(_key_handler) handler) override
        {
            _key_handler = std::move(handler);
        }
        void text_input_handler(decltype(_input_handler) handler) override
        {
            _input_handler = std::move(handler);
        }
    };
}

#endif
