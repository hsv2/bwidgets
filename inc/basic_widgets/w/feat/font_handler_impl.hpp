#ifndef BWIDGETS_FONT_HANDLER_IMPL_HPP
#define BWIDGETS_FONT_HANDLER_IMPL_HPP

#include <basic_widgets/w/feat/font_handler.hpp>

namespace bwidgets
{
    class FontHandlerImpl : public virtual FontHandler
    {
    public:
        void font(const std::shared_ptr<Font> f) final
        {
            if (f
                && (f != _font || f->family_name != _font->family_name
                    || f->height != _font->height || f->hinting() != _font->hinting()
                    || f->kerning() != _font->kerning()
                    || f->outline() != _font->outline()
                    || f->style_name != _font->style_name))
            {
                _handle_font_change(f);
                _font = f;
            }
        }

        void font_color_bg(const Color c) final
        {
            if (c != _font_color_bg) {
                _handle_font_color_change(_font_color_fg, c);
                _font_color_bg = c;
            }
        }

        void font_color_fg(const Color c) final
        {
            if (c != _font_color_fg) {
                _handle_font_color_change(c, _font_color_fg);
                _font_color_fg = c;
            }
        }

    protected:
        std::shared_ptr<Font> _font;
        Color                 _font_color_bg = default_font_color_bg;
        Color                 _font_color_fg = default_font_color_fg;
        Font::RenderMode      _font_render_mode {Font::RenderMode::SHADED};

        // Called on font changes.
        virtual void _handle_font_change(const std::shared_ptr<Font>&) = 0;
        // Called on font color changes.
        virtual void _handle_font_color_change(Color, Color)           = 0;
    };
}

#endif
