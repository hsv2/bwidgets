#ifndef BWIDGETS_BUTTON_IMPL_HPP
#define BWIDGETS_BUTTON_IMPL_HPP

#include <basic_widgets/w/base/widget_impl.hpp>
#include <basic_widgets/w/button.hpp>
#include <basic_widgets/w/caption.hpp>
#include <basic_widgets/w/feat/font_handler_impl.hpp>
#include <basic_widgets/w/feat/mouse_handler_impl.hpp>

namespace bwidgets
{
    class ButtonImpl : public virtual Button,
                       public virtual FontHandlerImpl,
                       public virtual MouseHandlerImpl,
                       public virtual WidgetImpl
    {
    public:
        ButtonImpl(Widget* parent = nullptr) noexcept;

        // Smallest valid button size.
        [[nodiscard]] auto size() const noexcept -> Size override;
        [[nodiscard]] auto text() const noexcept -> std::string_view override;
        void               text(std::string) override;

    protected:
        std::unique_ptr<Caption> _caption;
        SDL_Rect                 _caption_area {};
        Color                    _color_foreground = default_color_fg;

        void _handle_font_change(const std::shared_ptr<Font>&) override;
        void _handle_font_color_change(Color, Color)
        override;
        void _handle_geometry_change(const SDL_Rect&) override;
        void _handle_renderer_change(const std::shared_ptr<Renderer>&) override;
        void _handle_rendering() override;
        void _on_push(bool) override;
    };
}

#endif
