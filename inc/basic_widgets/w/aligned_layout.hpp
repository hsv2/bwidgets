#ifndef BWIDGETS_ALIGNED_LAYOUT_HPP
#define BWIDGETS_ALIGNED_LAYOUT_HPP

#include <basic_widgets/w/base/layout.hpp>

namespace bwidgets
{
    enum struct LayoutAlignment
    {
        HORIZONTAL,
        VERTICAL
    };

    // Align vertically or horizontally widgets.
    class AlignedLayout : public virtual Layout
    {
    public:
        using Layout::Layout;
    };
}

#endif
