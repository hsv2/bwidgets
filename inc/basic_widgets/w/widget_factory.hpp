#ifndef BWIDGETS_WIDGET_FACTORY_HPP
#define BWIDGETS_WIDGET_FACTORY_HPP

#include <basic_widgets/w/aligned_layout.hpp>
#include <basic_widgets/w/button.hpp>
#include <basic_widgets/w/caption.hpp>
#include <basic_widgets/w/numeric_input.hpp>

namespace bwidgets
{
    auto create_button(Widget* p = nullptr) -> std::unique_ptr<Button>;
    auto create_caption(Widget* p = nullptr) -> std::unique_ptr<Caption>;
    auto create_horizontal_layout(Widget* p = nullptr) -> std::unique_ptr<AlignedLayout>;
    auto create_input_float(Widget* p = nullptr) -> std::unique_ptr<NumericInput<float>>;
    auto create_input_int(Widget* p = nullptr) -> std::unique_ptr<NumericInput<int>>;
    auto create_vertical_layout(Widget* p = nullptr) -> std::unique_ptr<AlignedLayout>;
}

#endif
