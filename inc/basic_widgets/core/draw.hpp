#ifndef BWIDGETS_DRAW_HPP
#define BWIDGETS_DRAW_HPP

#include <functional>
#include <memory>

struct SDL_PixelFormat;
struct SDL_Point;

namespace bwidgets
{
    class Color;
    class Renderer;
    class Texture;

    // Add transparency to color base_color relative to the distance d from
    // a limit to produce an AntiAliasing effect.
    // d >= 0                → full transparency;
    // −(aa_pixels) <= d < 0 → smoothsteped transparency gradient;
    // d < −(aa_pixels)      → fully opaque.
    [[nodiscard]] auto aa(Color base_color, int aa_pixels, float d) noexcept -> Color;
    // Render a filled circle texture of color c and diameter resolution with
    // aa_pixels used for antialiasing.
    [[nodiscard]] auto filled_circle(Color c, int resolution, const Renderer&,
                                     int aa_pixels = 3) -> std::shared_ptr<Texture>;
    // Set the color of every pixels of a Texture using the passed function to compute
    // pixel color.
    void
    set_pixels_color(Texture*,
                     const std::function<uint32_t(SDL_Point, const SDL_PixelFormat&)>&);
}

#endif
