#ifndef BWIDGETS_FONT_HPP
#define BWIDGETS_FONT_HPP

#include <memory>
#include <string>

#include <SDL_ttf.h>

#include <basic_widgets/core/type/color.hpp>
#include <basic_widgets/core/type/deleter.hpp>
#include <basic_widgets/core/type/size.hpp>

namespace bwidgets
{
    // Wrap TTF_Font from SDL_ttf and provide a font finding function.
    class Font final
    {
        const std::unique_ptr<TTF_Font, Deleter> _data;

    public:
        enum struct Hinting
        {
            LIGHT  = TTF_HINTING_LIGHT,
            MONO   = TTF_HINTING_MONO,
            NONE   = TTF_HINTING_NONE,
            NORMAL = TTF_HINTING_NORMAL
        };

        enum struct RenderMode
        {
            SOLID,
            SHADED,
            BLENDED
        };

        enum struct Style
        {
            BOLD          = TTF_STYLE_BOLD,
            ITALIC        = TTF_STYLE_ITALIC,
            NORMAL        = TTF_STYLE_NORMAL,
            STRIKETHROUGH = TTF_STYLE_STRIKETHROUGH,
            UNDERLINE     = TTF_STYLE_UNDERLINE
        };

        static const Color default_color_bg;
        static const Color default_color_fg;

        const int         ascent;
        const int         descent;
        const long        faces;
        const std::string family_name;
        const bool        fixed_width;
        const int         height;
        const int         line_skip;
        const std::string style_name;

        explicit Font(TTF_Font*);
        // Load font at given path and with given size.
        Font(std::string_view, int);
        Font(const Font&) = delete;
        Font(Font&&)      = delete;
        ~Font() noexcept  = default;

        auto operator=(const Font&) = delete;
        auto operator=(Font&&) = delete;

        [[nodiscard]] auto hinting() const noexcept -> Hinting;
        auto               hinting(Hinting) noexcept -> Font*;
        [[nodiscard]] auto kerning() const noexcept -> bool;
        auto               kerning(bool) noexcept -> Font*;
        [[nodiscard]] auto outline() const noexcept -> int;
        auto               outline(int) noexcept -> Font*;
        auto render(RenderMode, std::string_view, Color fg = default_color_fg,
                    Color bg = default_color_bg)
          -> std::unique_ptr<SDL_Surface, Deleter>;
        [[nodiscard]] auto style() const noexcept -> uint8_t;
        auto               style(uint8_t) noexcept -> Font*;
        [[nodiscard]] auto text_size(std::string_view) const noexcept -> Size;

        // Get file path of the font best matching a give fontconfig pattern.
        [[nodiscard]] static auto find(std::string_view) -> std::string;
    };
}

#endif
