#ifndef BWIDGETS_SIZE_HPP
#define BWIDGETS_SIZE_HPP

#include <basic_widgets/core/type/concepts.hpp>

namespace bwidgets
{
    // Represent size of a 2D surface.
    struct Size
    {
        int w;
        int h;
    };

    // Addition the dimensions of two Size objects.
    [[nodiscard]] inline auto operator+(const Size a, const Size b) noexcept -> Size
    {
        return {a.w + b.w, a.h + b.h};
    }

    // Substract dimensions of a Size object from another.
    [[nodiscard]] inline auto operator-(const Size a, const Size b) noexcept -> Size
    {
        return {a.w - b.w, a.h - b.h};
    }

    // Multiply dimensions of a Size instance by a number.
    template<Numeric N>
    [[nodiscard]] inline auto operator*(const Size a, const N b) noexcept -> Size
    {
        return {a.w * b, a.h * b};
    }

    template<Numeric N>
    [[nodiscard]] inline auto operator*(const N a, const Size b) noexcept -> Size
    {
        return b * a;
    }
}

#endif
