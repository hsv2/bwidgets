#ifndef BWIDGETS_SDL_ERROR_HPP
#define BWIDGETS_SDL_ERROR_HPP

#include <basic_widgets/core/type/exception.hpp>

namespace bwidgets
{
    // Custom exception type for SDL errors.
    struct SDLError final : BaseException
    {
        using BaseException::BaseException;
    };
}

#endif
