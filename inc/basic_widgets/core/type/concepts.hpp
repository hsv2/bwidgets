#ifndef BWIDGETS_CONCEPTS_HPP
#define BWIDGETS_CONCEPTS_HPP

#include <string>
#include <type_traits>

namespace bwidgets
{
    // T can be converted to string with to_string.
    template<typename T>
    concept CanToString = requires(T value)
    {
        std::to_string(value);
    };

    // T is a floating point type.
    template<typename T>
    concept FloatingPoint = std::is_floating_point_v<T>;

    // T can be any numeric type.
    template<typename T>
    concept Numeric = std::is_arithmetic_v<T>;

    // T can be converted to string by using ostream.
    template<typename T>
    concept Printable = requires(T value)
    {
        std::declval<std::ostream&>() << value;
    };
}

#endif
