#ifndef BWIDGETS_FC_ERROR_HPP
#define BWIDGETS_FC_ERROR_HPP

#include <basic_widgets/core/type/exception.hpp>

namespace bwidgets
{
    // Exception type for fontconfig errors
    struct FCError final : BaseException
    {
        using BaseException::BaseException;
    };
}

#endif
