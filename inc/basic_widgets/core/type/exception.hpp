#ifndef BWIDGETS_EXCEPTION_HPP
#define BWIDGETS_EXCEPTION_HPP

#include <stdexcept>

#include <basic_widgets/core/type/concepts.hpp>

namespace bwidgets
{
    // Base type for custom runtime errors.
    class BaseException : std::runtime_error
    {
    protected:
        using runtime_error::runtime_error;
    };

    // T derives from BaseException.
    template<typename T>
    concept Exception = std::derived_from<T, BaseException>;
}

#endif
