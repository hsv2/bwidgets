#ifndef BWIDGETS_OPAQUE_STRUCT_HPP
#define BWIDGETS_OPAQUE_STRUCT_HPP

#include <fontconfig/fontconfig.h>
#include <SDL_render.h>
#include <SDL_surface.h>
#include <SDL_ttf.h>

namespace bwidgets
{
    // Deleter type for fancy pointers.
    struct Deleter
    {
        void operator()(FcConfig* ptr)
        {
            FcConfigDestroy(ptr);
        }

        void operator()(FcPattern* ptr)
        {
            FcPatternDestroy(ptr);
        }

        void operator()(SDL_Renderer* ptr)
        {
            SDL_DestroyRenderer(ptr);
        }

        void operator()(SDL_Surface* ptr)
        {
            SDL_FreeSurface(ptr);
        }

        void operator()(SDL_Texture* ptr)
        {
            SDL_DestroyTexture(ptr);
        }

        void operator()(SDL_Window* ptr)
        {
            SDL_DestroyWindow(ptr);
        }

        void operator()(TTF_Font* ptr)
        {
            TTF_CloseFont(ptr);
        }
    };
}

#endif
