#ifndef BWIDGETS_RENDERER_HPP
#define BWIDGETS_RENDERER_HPP

#include <memory>
#include <span>

#include <SDL_render.h>

#include <basic_widgets/core/error_helper.hpp>
#include <basic_widgets/core/type/color.hpp>
#include <basic_widgets/core/type/deleter.hpp>
#include <basic_widgets/core/type/size.hpp>

namespace bwidgets
{
    class Texture;

    // Wrap some of SDL's 2D accelerated rendering API.
    class Renderer final
    {
        friend Texture;

        const std::unique_ptr<SDL_Renderer, Deleter> _data;

        static auto _info(SDL_Renderer* r) -> SDL_RendererInfo
        {
            SDL_RendererInfo info;
            success_or_throw<SDLError>(SDL_GetRendererInfo(r, &info));

            return info;
        }

    public:
        const SDL_RendererInfo info;

        explicit Renderer(SDL_Renderer*);
        Renderer(SDL_Window*, int, uint32_t);
        Renderer(const Renderer&) = delete;
        Renderer(Renderer&&)      = delete;
        ~Renderer() noexcept      = default;

        auto operator=(const Renderer&) = delete;
        auto operator=(Renderer&&) = delete;

        [[nodiscard]] auto blend_mode() -> SDL_BlendMode;
        auto               blend_mode(SDL_BlendMode) -> Renderer*;
        auto               clear() -> Renderer*;
        auto copy(const Texture&, const SDL_Rect*, const SDL_Rect*) -> Renderer*;
        [[nodiscard]] auto draw_color() -> Color;
        auto               draw_color(Color) -> Renderer*;
        auto               draw_line(SDL_Point, SDL_Point) -> Renderer*;
        auto               draw_lines(std::span<SDL_Point>) -> Renderer*;
        auto               draw_point(SDL_Point) -> Renderer*;
        auto               draw_points(std::span<SDL_Point>) -> Renderer*;
        auto               draw_rect(const SDL_Rect*) -> Renderer*;
        auto               draw_rects(std::span<SDL_Rect>) -> Renderer*;
        auto               fill_rect(const SDL_Rect*) -> Renderer*;
        auto               fill_rects(std::span<SDL_Rect>) -> Renderer*;
        [[nodiscard]] auto output_size() -> Size;
        void               present() noexcept;
        [[nodiscard]] auto viewport() noexcept -> SDL_Rect;
        auto               viewport(const SDL_Rect*) -> Renderer*;

        auto copy(const Texture& t, const SDL_Rect* src, const SDL_Rect& dst)
        {
            const auto d = dst;
            return copy(t, src, &d);
        }
        auto copy(const Texture& t, const SDL_Rect& src, const SDL_Rect* dst)
        {
            const auto s = src;
            return copy(t, &s, dst);
        }
        auto copy(const Texture& t, const SDL_Rect& src, const SDL_Rect& dst)
        {
            const auto s = src;
            const auto d = dst;
            return copy(t, &s, &d);
        }
        auto draw_rect(SDL_Rect&& r)
        {
            const auto rect = r;
            return draw_rect(&rect);
        }
        auto fill_rect(SDL_Rect&& r)
        {
            const auto rect = r;
            return fill_rect(&rect);
        }
        auto viewport(SDL_Rect&& vp)
        {
            const auto v = vp;
            return viewport(&v);
        }
    };
}

#endif
