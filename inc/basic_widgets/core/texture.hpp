#ifndef BWIDGETS_TEXTURE_HPP
#define BWIDGETS_TEXTURE_HPP

#include <memory>

#include <SDL_pixels.h>
#include <SDL_render.h>

#include <basic_widgets/core/error_helper.hpp>
#include <basic_widgets/core/type/color.hpp>
#include <basic_widgets/core/type/deleter.hpp>

namespace bwidgets
{
    class Renderer;

    // Wrap most of the texture functions of SDL 2D rendering API.
    class Texture final
    {
        friend Renderer;

        struct Attr final
        {
            uint32_t          format_raw;
            SDL_PixelFormat*  format;
            SDL_TextureAccess access;
            int               w, h;
        };

        const Attr                                  _attributes;
        const std::unique_ptr<SDL_Texture, Deleter> _data;

    public:
        explicit Texture(SDL_Texture*);
        Texture(const Renderer&, SDL_PixelFormatEnum, SDL_TextureAccess, int, int);
        Texture(const Renderer&, SDL_Surface*);
        Texture(const Texture&) = delete;
        Texture(Texture&&)      = delete;
        ~Texture() noexcept;

        auto operator=(const Texture&) = delete;
        auto operator=(Texture&&) = delete;

        [[nodiscard]] auto alpha_mode() -> uint8_t;
        auto               alpha_mode(uint8_t) -> Texture*;
        [[nodiscard]] auto blend_mode() -> SDL_BlendMode;
        auto               blend_mode(SDL_BlendMode) -> Texture*;
        [[nodiscard]] auto color_mode() -> Color;
        auto               color_mode(Color) -> Texture*;
        [[nodiscard]] auto scale_mode() -> SDL_ScaleMode;
        auto               scale_mode(SDL_ScaleMode) -> Texture*;
        auto               update(const SDL_Rect*, const void*, int) -> Texture*;

        [[nodiscard]] const auto& attributes() const noexcept
        {
            return _attributes;
        }

        auto update(SDL_Rect&& r, const void* pix, int pitch)
        {
            SDL_Rect rect = r;
            update(&rect, pix, pitch);

            return this;
        }

        [[nodiscard]] static auto attributes(SDL_Texture* t)
        {
            Attr attr {};
            success_or_throw<SDLError>(
              SDL_QueryTexture(t, &attr.format_raw,
                               // NOLINTNEXTLINE(cppcoreguidelines-pro-type-cstyle-cast)
                               (int*)&attr.access, &attr.w, &attr.h));
            attr.format = ptr_or_throw<SDLError>(SDL_AllocFormat(attr.format_raw));

            return attr;
        }
    };
}

#endif
