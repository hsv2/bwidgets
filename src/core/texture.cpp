#include <basic_widgets/core/renderer.hpp>
#include <basic_widgets/core/texture.hpp>

using namespace bwidgets;

Texture::Texture(SDL_Texture* t)
  : _attributes(attributes(t)), _data {ptr_or_throw<SDLError>(t)}
{}

Texture::Texture(const Renderer& r, const SDL_PixelFormatEnum f,
                 const SDL_TextureAccess a, int w, int h)
  : Texture {SDL_CreateTexture(r._data.get(), f, a, w, h)}
{}

Texture::Texture(const Renderer& r, SDL_Surface* const s)
  : Texture {SDL_CreateTextureFromSurface(r._data.get(), s)}
{}

Texture::~Texture() noexcept
{
    // Idk why but freeing the format in ~Attr gives memory errors (valgrind)
    SDL_FreeFormat(_attributes.format);
}

auto Texture::alpha_mode() -> uint8_t
{
    uint8_t mode = 0;
    success_or_throw<SDLError>(SDL_GetTextureAlphaMod(_data.get(), &mode));

    return mode;
}

auto Texture::alpha_mode(const uint8_t m) -> Texture*
{
    success_or_throw<SDLError>(SDL_SetTextureAlphaMod(_data.get(), m));

    return this;
}

auto Texture::blend_mode() -> SDL_BlendMode
{
    SDL_BlendMode mode {};
    success_or_throw<SDLError>(SDL_GetTextureBlendMode(_data.get(), &mode));

    return mode;
}

auto Texture::blend_mode(const SDL_BlendMode m) -> Texture*
{
    success_or_throw<SDLError>(SDL_SetTextureBlendMode(_data.get(), m));

    return this;
}

auto Texture::color_mode() -> Color
{
    Color mode;
    success_or_throw<SDLError>(
      SDL_GetTextureColorMod(_data.get(), &mode().r, &mode().g, &mode().b));

    return mode;
}

auto Texture::color_mode(Color m) -> Texture*
{
    success_or_throw<SDLError>(SDL_SetTextureColorMod(_data.get(), m().r, m().g, m().b));

    return this;
}

auto Texture::scale_mode() -> SDL_ScaleMode
{
    SDL_ScaleMode mode {};
    success_or_throw<SDLError>(SDL_GetTextureScaleMode(_data.get(), &mode));

    return mode;
}

auto Texture::scale_mode(const SDL_ScaleMode m) -> Texture*
{
    success_or_throw<SDLError>(SDL_SetTextureScaleMode(_data.get(), m));

    return this;
}

auto Texture::update(const SDL_Rect* const r, const void* const pixels, const int pitch)
  -> Texture*
{
    success_or_throw<SDLError>(SDL_UpdateTexture(_data.get(), r, pixels, pitch));

    return this;
}
