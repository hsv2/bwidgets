#include <basic_widgets/w/base/layout_impl.hpp>

using namespace bwidgets;

void LayoutImpl::add_widget(std::unique_ptr<Widget> widget_ptr)
{
    if (_renderer) widget_ptr->renderer(_renderer);
    _widgets.emplace_back(std::move(widget_ptr));
    _update_layout(_viewport);
}

void LayoutImpl::for_widgets(const std::function<void(Widget*)>& f)
{
    for (const auto& w : _widgets) f(w.get());
}

void LayoutImpl::handle_event(const SDL_Event& ev)
{
    EventHandlerImpl::handle_event(ev);
    for (const auto& w : _widgets) w->handle_event(ev);
}

void LayoutImpl::_handle_geometry_change(const SDL_Rect& vp)
{
    _update_layout(vp);
}

void LayoutImpl::_handle_renderer_change(const std::shared_ptr<Renderer>& r)
{
    for (const auto& w : _widgets) w->renderer(r);
}

void LayoutImpl::_handle_rendering()
{
    for (const auto& w : _widgets) w->render();
}
