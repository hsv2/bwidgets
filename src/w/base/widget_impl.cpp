#include <basic_widgets/w/base/widget_impl.hpp>

using namespace bwidgets;

void WidgetImpl::render()
{
    if (!_renderer) return;

#ifdef BWIDGETS_DEBUG
    // Render a debug outline of widget viewport.
    _renderer
      ->draw_color({
  // NOLINTNEXTLINE(readability-magic-numbers)
        {0, 255, 0, SDL_ALPHA_OPAQUE}
    })
      ->draw_rect(nullptr);
#endif

    _renderer->viewport(&_viewport);
    _handle_rendering();
}

void WidgetImpl::renderer(std::shared_ptr<Renderer> r)
{
    if (r != _renderer) {
        _handle_renderer_change(r);
        _renderer = std::move(r);
    }
}

void WidgetImpl::viewport(const SDL_Rect& vp)
{
    _handle_geometry_change(vp);
    _viewport = vp;
}

auto WidgetImpl::viewport() const -> const SDL_Rect&
{
    return _viewport;
}
