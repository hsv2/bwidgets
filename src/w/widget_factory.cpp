#include <basic_widgets/w/aligned_layout_impl.hpp>
#include <basic_widgets/w/button_impl.hpp>
#include <basic_widgets/w/caption_impl.hpp>
#include <basic_widgets/w/numeric_input_impl.hpp>
#include <basic_widgets/w/widget_factory.hpp>

auto bwidgets::create_horizontal_layout(Widget* parent) -> std::unique_ptr<AlignedLayout>
{
    return std::unique_ptr<AlignedLayout>(
      new AlignedLayoutImpl<LayoutAlignment::HORIZONTAL>(parent));
}

auto bwidgets::create_vertical_layout(Widget* parent) -> std::unique_ptr<AlignedLayout>
{
    return std::unique_ptr<AlignedLayout>(
      new AlignedLayoutImpl<LayoutAlignment::VERTICAL>(parent));
}

auto bwidgets::create_button(Widget* parent) -> std::unique_ptr<Button>
{
    return std::unique_ptr<Button>(new ButtonImpl(parent));
}

auto bwidgets::create_caption(Widget* parent) -> std::unique_ptr<Caption>
{
    return std::unique_ptr<Caption>(new CaptionImpl(parent));
}

auto bwidgets::create_input_float(Widget* parent) -> std::unique_ptr<NumericInput<float>>
{
    return std::unique_ptr<NumericInput<float>>(new NumericInputImpl<float>(parent));
}

auto bwidgets::create_input_int(Widget* parent) -> std::unique_ptr<NumericInput<int>>
{
    return std::unique_ptr<NumericInput<int>>(new NumericInputImpl<int>(parent));
}
