#include <basic_widgets/w/feat/keyboard_handler_impl.hpp>

using namespace bwidgets;

void KeyboardHandlerImpl::disable_keyboard_handler()
{
    _remove_event_handler(SDL_KEYDOWN);
    _remove_event_handler(SDL_KEYUP);
    _remove_event_handler(SDL_TEXTINPUT);
}

void KeyboardHandlerImpl::enable_keyboard_handler()
{
    const auto keyboard_handler = [this](const SDL_Event& ev) {
        if (focus()) _key_handler(ev.key);
    };

    _add_event_handler({SDL_KEYDOWN, keyboard_handler});
    _add_event_handler({SDL_KEYUP, keyboard_handler});
    _add_event_handler({SDL_TEXTINPUT, [this](const SDL_Event& ev) {
                            if (focus()) _input_handler(ev.text);
                        }});
}
