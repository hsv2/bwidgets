#include <basic_widgets/w/feat/event_handler_impl.hpp>

using namespace bwidgets;

void EventHandlerImpl::handle_event(const SDL_Event& ev)
{
    auto type = static_cast<SDL_EventType>(ev.type);
    if (!_event_handlers.contains(type)) return;

    _event_handlers[type](ev);
}

auto EventHandlerImpl::_add_event_handler(handler_t event_handler) -> bool
{
    if (_event_handlers.contains(event_handler.first)) return false;

    _event_handlers.emplace(std::move(event_handler));
    return true;
}

auto EventHandlerImpl::_remove_event_handler(const SDL_EventType ev_type)
  -> std::pair<handler_t, bool>
{
    handler_t handler;
    auto      pos = _event_handlers.find(ev_type);
    if (pos == _event_handlers.end()) return {handler, false};

    handler = {ev_type, _event_handlers[ev_type]};
    _event_handlers.erase(pos);

    return {handler, true};
}
