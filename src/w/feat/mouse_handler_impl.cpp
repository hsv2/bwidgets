#include <basic_widgets/core/math.hpp>
#include <basic_widgets/w/feat/mouse_handler_impl.hpp>

using namespace bwidgets;

void MouseHandlerImpl::disable_mouse_handler()
{
    _remove_event_handler(SDL_MOUSEBUTTONDOWN);
    _remove_event_handler(SDL_MOUSEBUTTONUP);
    _remove_event_handler(SDL_MOUSEMOTION);
}

// NOLINTNEXTLINE(readability-function-cognitive-complexity)
void MouseHandlerImpl::enable_mouse_handler(const SDL_Rect& rel_area,
                                            const SDL_Rect& origin)
{
    const auto mouse_in_rect = [&rel_area, &origin](const SDL_Point p) {
        const auto area = rect_offset(rel_area, origin);
        return SDL_PointInRect(&p, &area) == SDL_TRUE;
    };

    _add_event_handler({SDL_MOUSEBUTTONDOWN, [this, mouse_in_rect](const SDL_Event& ev) {
                            if (!mouse_in_rect({ev.button.x, ev.button.y})) {
                                focus(false);
                                return;
                            }
                            _on_push(_pushed = true);
                        }});
    _add_event_handler({SDL_MOUSEBUTTONUP, [this, mouse_in_rect](const SDL_Event& ev) {
                            if (_pushed) {
                                _on_push(_pushed = false);
                                // Only execute handler and give focus if mouse button
                                // has been pushed and released over the widget.
                                if (mouse_in_rect({ev.button.x, ev.button.y})) {
                                    focus(true);
                                    _click_handler(ev.button);
                                }
                            }
                        }});
    _add_event_handler({SDL_MOUSEMOTION, [this, mouse_in_rect](const SDL_Event& ev) {
                            if (!mouse_in_rect({ev.motion.x, ev.motion.y})) {
                                if (_hovered) _hover_handler(_hovered = false);
                                return;
                            }
                            if (!_hovered) _hover_handler(_hovered = true);
                            _motion_handler(ev.motion);
                        }});
}
