#ifndef BWIDGETS_EXAMPLES_RUN_HPP
#define BWIDGETS_EXAMPLES_RUN_HPP

#include <functional>
#include <iostream>

#include <basic_widgets/core/type/deleter.hpp>
#include <basic_widgets/w/widget_factory.hpp>

template<typename T>
concept WidgetType = std::derived_from<T, bwidgets::Widget>;

template<WidgetType W>
void run_example(
  std::function<std::unique_ptr<W>()>                                      factory,
  std::function<void(W*, const std::shared_ptr<bwidgets::Font>, int, int)> setup,
  int w = 3, int h = 3)
{
    std::atexit([]() {
        TTF_Quit();
        SDL_Quit();
    });
    try {
        bwidgets::success_or_throw<bwidgets::SDLError>(SDL_Init(SDL_INIT_VIDEO));
        bwidgets::success_or_throw<bwidgets::SDLError>(TTF_Init());
        const bwidgets::Size size_init {854, 480};
        auto                 font =
          std::make_shared<bwidgets::Font>(bwidgets::Font::find("Monospace"),
                                           16); // NOLINT(readability-magic-numbers)

        auto win = std::unique_ptr<SDL_Window, bwidgets::Deleter>(
          bwidgets::ptr_or_throw<bwidgets::SDLError>(SDL_CreateWindow(
            "basic_widgets example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            size_init.w, size_init.h,
            SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_UTILITY)));

        auto renderer =
          std::make_shared<bwidgets::Renderer>(win.get(), -1, SDL_RENDERER_ACCELERATED);
        renderer->blend_mode(SDL_BLENDMODE_BLEND);

        auto layout = bwidgets::create_horizontal_layout();
        for (auto x = 0; x < w; x++) {
            auto col = bwidgets::create_vertical_layout();
            for (auto y = 0; y < h; y++) {
                auto widget = factory();
                setup(widget.get(), font, x, y);
                col->add_widget(std::move(widget));
            }
            layout->add_widget(std::move(col));
        }

        layout->renderer(renderer);
        layout->viewport({0, 0, size_init.w, size_init.h});

        bool quit {false};
        while (!quit) {
            SDL_Event ev;
            while (SDL_PollEvent(&ev) != 0) {
                switch (ev.type) {
                    case SDL_QUIT:
                        quit = true;
                        break;
                    case SDL_WINDOWEVENT:
                        if (ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                            const auto size = renderer->output_size();
                            layout->viewport({0, 0, size.w, size.h});
                        }
                        break;
                }
                layout->handle_event(ev);
            }

            // NOLINTNEXTLINE(readability-magic-numbers)
            renderer->draw_color({50, 60, 70, SDL_ALPHA_OPAQUE});
            renderer->clear();

            layout->render();
            renderer->present();
        }
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}

#endif
