#include <basic_widgets/w/widget_factory.hpp>

#include "run.hpp"

auto main() -> int
{
    run_example<bwidgets::Caption>([]() { return bwidgets::create_caption(); },
                                   [](auto w, auto f, auto, auto) {
                                       w->alignment =
                                         bwidgets::Caption::Alignment::CENTER;
                                       w->text("¡jello!");
                                       w->font(f);
                                   },
                                   4, 8); // NOLINT(readability-magic-numbers)
    return 0;
}
