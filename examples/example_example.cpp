#include "example_widget.hpp"
#include "run.hpp"

auto main() -> int
{
    run_example<Example>(
      []() { return std::make_unique<Example>(); },
      [](auto w, auto, auto x, auto y) {
          w->cycle_r = (x + 1) * 3000;              // NOLINT(readability-magic-numbers)
          w->cycle_b = (y + 1) * 3000;              // NOLINT(readability-magic-numbers)
          w->cycle_b = (1 + x + y) * (y + 1) * 400; // NOLINT(readability-magic-numbers)
      });
}
