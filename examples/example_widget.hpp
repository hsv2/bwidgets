#ifndef BWIDGETS_EXAMPLES_EXAMPLE_WIDGET
#define BWIDGETS_EXAMPLES_EXAMPLE_WIDGET

#include <chrono>

#include <basic_widgets/core/math.hpp>
#include <basic_widgets/core/renderer.hpp>
#include <basic_widgets/w/base/widget_impl.hpp>

using bwidgets::Color;
using bwidgets::rect_margin;
using bwidgets::Size;
using bwidgets::WidgetImpl;

class Example final : public WidgetImpl
{
    void _handle_geometry_change(const SDL_Rect& vp) noexcept override
    {
        _widget_area = {2, 2, vp.w - 4, vp.h - 4};
    }

    void _handle_rendering() override
    {
        const auto now = std::chrono::duration_cast<std::chrono::milliseconds>(
                           std::chrono::steady_clock::now().time_since_epoch())
                           .count();
        const uint8_t r = 255 * (now % cycle_r / (float)cycle_r); // NOLINT
        const uint8_t g = 255 * (now % cycle_g / (float)cycle_g); // NOLINT
        const uint8_t b = 255 * (now % cycle_b / (float)cycle_b); // NOLINT
        const Color   base_color {r, g, b, SDL_ALPHA_OPAQUE};

        const int border = 10;
        for (auto i = 0; i < border; i += 3) {
            uint8_t alpha = 255 * i / border; // NOLINT(readability-magic-numbers)
            _renderer
              ->draw_color({
                {base_color().r, base_color().g, base_color().b, alpha}
            })
              ->draw_rect(rect_margin(_widget_area, {i, i}));
        }

        _renderer->draw_color(base_color)
          ->draw_rect(nullptr)
          ->fill_rect(rect_margin(_widget_area, {border * 2, border * 2}));
    }

public:
    unsigned int cycle_r {3500}; // NOLINT(readability-magic-numbers)
    unsigned int cycle_g {3500}; // NOLINT(readability-magic-numbers)
    unsigned int cycle_b {3500}; // NOLINT(readability-magic-numbers)

    using WidgetImpl::WidgetImpl;

    [[nodiscard]] auto size() const noexcept -> Size override
    {
        return {128, 64}; // NOLINT(readability-magic-numbers)
    }
};

#endif
