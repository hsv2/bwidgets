#include <iostream>

#include <basic_widgets/w/widget_factory.hpp>

#include "run.hpp"

auto main() -> int
{
    run_example<bwidgets::Button>(
      []() { return bwidgets::create_button(); },
      [](auto w, auto f, auto x, auto y) {
          w->click_handler([x, y](const SDL_MouseButtonEvent&) {
              std::cout << "button(" << x << ',' << y << "):click!" << std::endl;
          });
          w->font(f);
          w->text("click me");
      });
    return 0;
}
