#include <basic_widgets/w/widget_factory.hpp>

#include "run.hpp"

auto main() -> int
{
    run_example<bwidgets::NumericInput<float>>(
      []() { return bwidgets::create_input_float(); },
      [](auto w, auto f, auto, auto) {
          w->font(f);
          w->button_step = 0.5;       // NOLINT(readability-magic-numbers)
          w->value_range(-3.14, 8.5); // NOLINT(readability-magic-numbers)
      });
    return 0;
}
