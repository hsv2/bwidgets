#!/usr/bin/env python3

from concurrent.futures import ProcessPoolExecutor
from pathlib import Path, PurePath
from subprocess import CompletedProcess, PIPE, STDOUT, run

files = sorted(Path().rglob("*.?pp"))


def tidy(file: PurePath) -> CompletedProcess:
    return run(
        ["clang-tidy", "-p", "build", "--use-color", "--quiet", file], stdout=PIPE, stderr=STDOUT,
        check=False
    )


def main():
    with ProcessPoolExecutor() as executor:
        for task_done in executor.map(tidy, files):
            print(task_done.stdout.decode())


if __name__ == "__main__":
    main()
